
var tabrat = require( "./tabrat" )
var Q = require( "Q" )

// global variables where you'll keep arrays of objects for later processing

var mysites
var myusers
var myprojects

// chain of calls to the REST API

tabrat.init()
.then( function( done ) {
	try {
		return tabrat.process_commands( "\
			sites\
			users\
			projects\
" )
	} catch( error ) { console.log( error ); process.exit() }
} )
.then( function( done ) {
	console.log( "Sites:" )
	console.log( tabrat.get_relative_frame( -2 ) )
	console.log( "Users:" )
	console.log( tabrat.get_relative_frame( -1 ) )
	console.log( "Projects:" )
	console.log( tabrat.get_relative_frame( 0 ) )
	process.exit()
})