
var tabrat = require( "./tabrat" )
var Q = require( "q" )

// chain of calls to the REST API

tabrat.init()
.then( function( sites ) {
	return tabrat.process_commands( "\
		createsite \"1000users\"\
		use \"1000users\"\
" ) } )
.then( function( sites ) {
	// you want to try / catch your code here otherwise you won't see errors
	try {
		var deferred = Q.defer()
		var promises = []
		for( i = 1; i<=1000; i++ ) {
			promises.push( tabrat.createuser( "user" + i ) );
		}
		Q.all( promises ).done( function( results ) {
			deferred.resolve( "done" )
		})
		return deferred.promise;
	} catch( error ) { console.log( error ); process.exit() }
})
.then( function( sites ) {
	console.log( "Done creating 1000 users" )
	process.exit()
})
