
var tabrat = require( "./tabrat" )
var Q = require( "Q" )

// global variables where you'll keep arrays of objects for later processing

var mysites
var myusers
var myprojects

// chain of calls to the REST API

tabrat.init()
.then( function( sites ) {
	Q.all( [ tabrat.sites(), tabrat.users(), tabrat.projects() ] ).done( function( results ) {
		try {
			console.log( "Here are the sites:" )
			console.log( results[0] )
			console.log( "Here are the users:" )
			console.log( results[1] )
			console.log( "Here are the projects:" )
			console.log( results[2] )
			process.exit()
		} catch( error ) { console.log( error ); process.exit() }
	} )
})