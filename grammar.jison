

/* lexical grammar */
%lex

id        			  [a-zA-Z]+

DecimalDigits [0-9]+

LineContinuation \\(\r\n|\r|\n)
OctalEscapeSequence (?:[1-7][0-7]{0,2}|[0-7]{2,3})
HexEscapeSequence [x]{HexDigit}{2}
UnicodeEscapeSequence [u]{HexDigit}{4}
SingleEscapeCharacter [\'\"\\bfnrtv]
NonEscapeCharacter [^\'\"\\bfnrtv0-9xu]
CharacterEscapeSequence {SingleEscapeCharacter}|{NonEscapeCharacter}
EscapeSequence {CharacterEscapeSequence}|{OctalEscapeSequence}|{HexEscapeSequence}|{UnicodeEscapeSequence}
DoubleStringCharacter ([^\"\\\n\r]+)|(\\{EscapeSequence})|{LineContinuation}
SingleStringCharacter ([^\'\\\n\r]+)|(\\{EscapeSequence})|{LineContinuation}
StringLiteral (\"{DoubleStringCharacter}*\")|(\'{SingleStringCharacter}*\')

%%
"{%".*"%}"		 	  return 'JAVASCRIPT';
"use"				  return 'USE';
"help"				  return 'HELP';
"exit"				  return 'EXIT';
"parameter"			  return 'PARAMETER';
"for"                 return 'FOR';
"frames"			  return 'FRAMES';
"filter"              return 'FILTER';
"collect"			  return 'COLLECT';
"sites"				  return 'SITES';
"users"				  return 'USERS';
"datasources"		  return 'DATASOURCES';
"workbooks"			  return 'WORKBOOKS';
"projects"			  return 'PROJECTS';
"print"				  return 'PRINT';
"download"			  return 'DOWNLOAD';
"publishworkbook"	  return 'PUBLISHWORKBOOK';
"createsite"		  return 'CREATESITE';
"adduser"             return 'ADDUSER';
"deletesite"		  return 'DELETESITE';
"createproject"		  return 'CREATEPROJECT';
"deleteproject"		  return 'DELETEPROJECT';
"deletetag"			  return 'DELETETAG';
"in"                  return 'IN';
"#-"				  return 'PASTFRAME'
"#"                   return 'FRAME';
{id}				  return 'ID';
{StringLiteral}		  return 'STRINGLITERAL';
{DecimalDigits}		  return 'DECIMALDIGITS';
"$"                   return 'DOLLAR';
"("                   return 'PARENTHESISLEFT';
")"                   return 'PARENTHESISRIGHT';
"{"					  return 'BRACELEFT';
"}"					  return 'BRACERIGHT';
"["					  return 'BRACKETLEFT';
"]"					  return 'BRACKETRIGHT';
"-"					  return '-';
"="					  return 'EQUALS';
","				      return 'COMMA';
\s+                   /* skip whitespace */
<<EOF>>               return 'EOF';

/lex

/* operator associations and precedence */

/* %left '+' '-' */
/* %left '*' '/' */
/* %left '^' */
/* %left UMINUS */

%start program

%% /* language grammar */

program
	: commands EOF			{ return $1; }
	;

commands
    : commands command 		{ $commands.push( $command ); $$ = $commands;  }
    | command				{ $$ = [$command] }
    ;

command
	: forloop				
	| listcommand 				
	| HELP					{ $$ = { "command": "H" }; }
	| EXIT					{ $$ = { "command": "X" }; }
	| FRAMES				{ $$ = { "command": "F" }; }
	| print
	| parameterdecl
	| javascript
	| assign
	| download
	| use
	| PUBLISHWORKBOOK value				{ $$ = { "command" : "publishworkbook", "workbook" : $2 }}
	| CREATESITE value					{ $$ = { "command" : "createsite", "site" : $2 }}
	| ADDUSER value value value value value		{ $$ = { "command" : "adduser", "user" : $2, "role": $3, "publish" : $4, "contentadmin" : $5, "siteadmin" : $6 }}
	| DELETESITE value					{ $$ = { "command" : "deletesite", "site" : $2 }}
	| CREATEPROJECT value value			{ $$ = { "command" : "createproject", "name" : $2, "description" : $3 }}
	| DELETEPROJECT value				{ $$ = { "command" : "deleteproject", "name" : $2 }}
	| DELETETAG value value				{ $$ = { "command" : "deletetag", "workbook_id" : $2, "tag" : $3 }}
	;

use
	: USE value							{ $$ = { "command" : "use", "site": $2 }}
	;

download
	: DOWNLOAD value					{ $$ = { "command" : "download", "value": $2 }}
	;

assign
	: DOLLAR ID EQUALS listcommand		{ $$ = { "command" : "assign", "variable":$2, "listcommand": $listcommand  } }
	;

print
	: PRINT value						{ $$ = { "command" : "print", "value" : $2 }}
	;

listcommand
	: filter
	| collect
	| workbooks
	| USERS					{ $$ = { "command": "LU" }; }
	| SITES					{ $$ = { "command": "LS" }; }
	| DATASOURCES			{ $$ = { "command": "LD" }; }
	| PROJECTS				{ $$ = { "command": "LP" }; }
	| frame
	;

workbooks
	: WORKBOOKS value 		{ $$ = { "command" : "WORKBOOKS", "user": $2 } }
	;

value
	: STRINGLITERAL 		{ $$ = { "value" : "string", "literal" : eval( $1 ) } }
	| element
	| javascript
	| variable
	| parameter
	| frame
	;

parameter
	: DOLLAR DECIMALDIGITS { $$ =  { "command" : "parameter", "parameter" : $2 } }
	;

parameterdecl
	: PARAMETER PARENTHESISLEFT DOLLAR DECIMALDIGITS COMMA STRINGLITERAL COMMA STRINGLITERAL PARENTHESISRIGHT { $$ = { "parameter" : $4, "question":eval($6), "default":eval($8) } }
	;

forloop
	: FOR DOLLAR ID IN frameorvariable oneorseveralcommands		{ $$ = { "command" : "loop", "variable" : $3, "framecommand" : $5, "commands" : $oneorseveralcommands }; }
	;

oneorseveralcommands
	: BRACELEFT commands BRACERIGHT  { $$ = $commands }
	| command 						 { $$ = [$command] }
	;

filter
	: FILTER DOLLAR ID IN frameorvariable javascript 						{ $$ = { "command" : "filter", "variable" : $3, "framecommand" : $frameorvariable, "javascript" : $6 }; }
	;

collect
	: COLLECT DOLLAR ID IN frameorvariable javascript 						{ $$ = { "command" : "collect", "variable" : $3, "framecommand" : $frameorvariable, "javascript" : $6 }; }
	;

javascript
	: JAVASCRIPT 			{ $$ = { "command" : "javascript", "code": $1.replace( '{%', '' ).replace( '%}', '' ) } }
	;

element
	: frame BRACKETLEFT DECIMALDIGITS BRACKETRIGHT		{ $$ = { "command" : "element", "framecommand": $frame, "index" : parseInt( $3 ) }; }
	;

variable
	: DOLLAR ID 			{ $$ = { "value" : "variable", "variable" : $2 }; }
	;

frame
	: FRAME DECIMALDIGITS			{ $$ = { "command": "frame", "frame": $2 }; }
	| PASTFRAME DECIMALDIGITS		{ $$ = { "command": "pastframe", "frame": $2 }; }
	;

frameorvariable
	: frame
	| variable
	;
