
TABRAT
======

Tabrat is a TABleau Rest Api Tool. With tabrat you work with the Tableau REST API directly from the command line, from a script, or from a node.js program without having to write a single HTTP call. 

The Tableau REST API lets you manage content and users of a Tableau Server with HTTP commands.

### About the Tableau REST API

See this [video intro](http://www.tableausoftware.com/learn/tutorials/on-demand/rest-api) on tableausoftware.com  
Read the [REST API online documentation](http://onlinehelp.tableausoftware.com/v8.2/server/en-us/help.htm#rest_api.htm%3FTocPath%3DREST%2520API%7C_____0) on tableausoftware.com

Configuring tabrat
------------------

Please modify the settings.json file and use your own server URL, credentials, path to Tableau Server. Here is the content of the sttings.json file as shipped:

    {
        "host" : "localhost",
        "port" : 80,
        "scheme" : "http://",
        "site" : "Default",
        "user" : "admin",
        "passwd" : "admin",
        "binfolder" : "C:\\Program Files\\Tableau\\Tableau Server\\8.2\\bin"
    }

Running tabrat
--------------

### Running from the command line


From the tabrat folder, try:

> node tabrat

you should see

    #####  ##  ###  ###   ##  #####
      #   #  # #  # #  # #  #   #
      #   #### ###  ###  ####   #
      #   #  # #  # #  # #  #   #
      #   #  # ###  #  # #  #   #
    
    TABleau Rest Api Tool
    4 frames - type 'help' for help

You can now test a few things. Type:

    users

On my machine, tabrat shows the following:

    #4 #-0 13 users
    1> admin
    2> aguinebertiere@tableausoftware.com
    3> chris
    4> Denny
    5> erin
    6> Fiona
    7> mark
    8> pat
    9> Rick
    10> sam
    11> Tableau Software
    12> william
    13> yodle
    5 frames - type 'help' for help

Note the first line:

    #4 #-0 13 users

The `#4` indicates that the result has been stored in frame number 4. The relative frame number is currently `#-0` (`#-0` is always the last frame). Finally, this frames contains `13 users`.

Now type

    exit

to exit the command line interface.

### Running a sample script

From the tabrat folder, try the following:

    node tabrat sample.tr

This should produce a list of all sites, users, and projects. Note than only projects in which the connection user has published content will be shown.

### Running a sample node program

From the tabrat folder, type this:

    node sample.js

This, just like the sample script, should produce a list of sites, users, and projects.  Note than only projects in which the connection user has published content will be shown.

Warning
-------

This code is freely distributable.
This utility does not carry any warranty from Tableau Software to be fit for any purpose.
Use at your own risk.

