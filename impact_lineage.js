
var tabrat = require( "./tabrat" )
var Q = require( "Q" )
var sys = require('sys')
var exec = require('child_process').exec;
var fs = require('fs');
var xml2js = require('xml2js');

// global variables where you'll keep arrays of objects for later processing

var mysites
var myusers
var myprojects

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

// chain of calls to the REST API

tabrat.init()
.then( function( sites ) {
	mysites = sites
	return tabrat.users()
})
.then( function( users ) {
	myusers = users
	try {
		console.log( "Fetching workbooks for each user" )
		var promises = []
		for( u in myusers ) (function( user ){
			console.log( " === " + user + " ===" )
			userobject = myusers[ user ]
			promises.push( tabrat.workbooks( user, true, false ) )
		})( u )
		Q.all( promises ).done( function( answers ) {
			// answers is an array of answers to each workbooks() call, eahc one being an array of workbooks
			var allworkbooks = []
			var downloadpromises = []
			for( a in answers ) {
				// console.log( answers[a] )
				for( w in answers[a] ) {
					console.log( answers[a][w].$.name + " (" + answers[a][w].views.length + " views)" )
					downloadpromises.push( tabrat.downloadWorkbook( answers[a][w].$.name ) )
					allworkbooks.push( answers[a][w] )
				}
			}
			Q.allSettled( downloadpromises ).done( function( answers ) {
				// unzip each download if necessary
				var zipdone = Q.defer()
				var child = exec( "\"c:\\Program Files\\7-Zip\\7z.exe\" x -y *.twbx", { cwd: "c:\\users\\aguinebertiere\\dev\\tabrat\\content" }, function( error, stdout, stderr) {
				   if( error != null ) {
				        console.log(stderr);
				        // error handling & exit
				   }
				   zipdone.fulfill( "done unzipping" )
				})
				zipdone.promise.then( function() {
					console.log( "Unzips finished" )

					var xmlpromises = []
					// going through file to parse them
					// we can't use answers because A.twbx may unpack to B.twb

					var files = fs.readdirSync( "content" )

					for( f in files ) {
						if( files[f].endsWith( ".twb" ) ) {
							workbookfilename = files[f]
							console.log( "Reading " + workbookfilename )
							var parser = new xml2js.Parser();
							var deferred = Q.defer();
							(function( def, filename ) {
							fs.readFile( "content/" + filename, function(err, data) {
								if( err ) {
									console.log( "Error reading file - " + err )
									def.reject( "Error reading file" )
								} else {
								    parser.parseString(data, function (err, result) {
								    	if( err ) console.log( "Error: " + err )
								    	var datasources = result.workbook.datasources[ 0 ].datasource
								    	for( d in datasources ) {
								    		if( datasources[d][ "repository-location" ] ) { // this is a published data source
								    			console.dir( datasources[d][ "repository-location" ][0] )
								    		}
								    	}
								        console.log( 'Done' );
								        def.resolve( "Done parsing" )
								    });
								}
							})
							})( deferred, workbookfilename );
							xmlpromises.push( deferred.promise )
						}
					}

					Q.all( xmlpromises ).done( function( answers ) {
						console.log( "Done parsing" )
						process.exit()
					})
				})
			})
			
		} )
	} catch( error ) { console.log( error ); process.exit() }
})