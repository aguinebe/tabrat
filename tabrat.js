
//
//
// Alexis Guinebertiere, 2014
// This code is freely distributable.
// This utility does not carry any warranty from Tableau Software to be fit for any purpose.
// Use at your own risk.
//
//

//
// GLOBAL SETTINGS
//

// these settings will be either read from the settings.json file, or set programmatically with setting(...)

var _scheme = ""
var _host = ""
var _port = 0
var _site = ""
var _admin = ""
var _passwd = ""
var _binfolder = ""

function settings( dictionnary ) {
	for(var attributename in dictionnary) {
		switch( attributename ) {
			case "scheme": _scheme = dictionnary[ "scheme" ]
			case "host": _host = dictionnary[ "host" ]
			case "port": _port = dictionnary[ "port" ]
			case "site": _site = (dictionnary[ "site" ] == 'Default' ? "" : dictionnary["site"] )
			case "user": _admin = dictionnary[ "user" ]
			case "passwd": _passwd = dictionnary[ "passwd" ]
			case "binfolder": _binfolder = dictionnary[ "binfolder" ]
			case "proxy": request = require( 'request' ).defaults({'proxy':dictionnary[ "proxy" ]})
		}
	}
}

exports.settings = settings

//
// LIBRARIES
//

var http = require( "http" )
var request = require('request')

var bl = require( "bl" )
var xml2js = require( "xml2js" )
var Q = require( "q" )
var rl = require( "readline" );
// var parser = require( "./grammar")
var parser = require( "./grammar" ).parser
var fs = require( "fs" )
var promptly = require( "promptly" )
var assert = require( "assert" )
var child_process = require('child_process')

//
// GLOBAL VARIABLES
//

var _token = ""
var _sites_by_name = {}
var _sites_by_URL = {}
var _site_objects_by_name = {}
var _projects_by_name = {}
var _users_by_name = {}
var _datasources_by_name = {}
var _projects = []
var _frames = []
var _variables = {}
var _parameters = {}

//
// HTTP COMMANDS
//

function baseurl() {
	return _scheme + _host + ":" + _port.toString()
}

function doget( path, callback ) {
	var completeUrl = baseurl() + path
	var cookie = "workgroup_session_id=" + _token
	request.get( { url : completeUrl, headers : { "X-Tableau-Auth" : _token, "Cookie" : cookie }, encoding: null }, function( error, response, body ) {
		if( error ) callback( error )
		else callback( null, body, response )
	})
}

function call_api_and_parse( verb, path, message, usetoken, callback ) {
	var completeUrl = baseurl() + path
	var cookie = "workgroup_session_id=" + _token
	var headers = {}
	if( usetoken ) { headers = { "X-Tableau-Auth" : _token, "Cookie" : cookie } }
	verb( { url : completeUrl, body: message, headers : headers, encoding: null }, function( error, response, body ) {
		if( error ) callback( { $:{ code: 1 }, detail: [ error ] } ) // http level error
		else xml2js.parseString( body, function( err, result ) {
			if( err ) callback( { $:{ code: 2 }, detail: [ err ] } ) // xml parsing error
			else {
				if( result && result.tsResponse && result.tsResponse.error ) callback( result.tsResponse.error[0] )
				else {
					if( response.statusCode > 300 ) callback( { $:{ code: response.statusCode }, detail: [ "HTTP resultCode = " + response.statusCode ] } )
					else callback( null, body, response, result )
				}
			} 
		})
	})
}

//
// READ SETTINGS FROM FILE
//

function read_settings_file() {
	var deferred = Q.defer();
	fs.readFile( __dirname + '/settings.json', function (err, data) {
	  if (err) {
	    throw err;
	  }
	  try {
	  	settings_json = JSON.parse( data )
	  	settings( settings_json )
	  	deferred.resolve( "done" )
	  }
	  catch( error ) {
	  	console.log( "Error parsing the settings file" )
	  	console.log( error )
	  	deferred.reject( "couldnt read settings" )
	  }
	})
	return deferred.promise
}

exports.read_settings_file = read_settings_file


//
//
//

function get_last_frame() {
	return _frames[ _frames.length - 1 ][ "content" ]
}

function get_relative_frame( offset_from_end ) {
	return _frames[ _frames.length - 1 + offset_from_end ][ "content" ]
}

exports.get_last_frame = get_last_frame
exports.get_relative_frame = get_relative_frame

//
// rest api call: SIGN IN
// returns a promise
//

function signin() {
	var deferred = Q.defer()
	message = "<tsRequest> \
		<credentials name=\"" + _admin + "\" password=\"" + _passwd + "\" > \
			<site contentUrl=\"" + _site + "\" /> \
		</credentials> \
	</tsRequest>"
	call_api_and_parse( request.post, "/api/2.0/auth/signin", message, false, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
    		_token = result.tsResponse.credentials[ 0 ].$.token
    		deferred.resolve( _token )
    	}
	})
	return deferred.promise
}

function signin_and_sites_and_users() {
	var deferred = Q.defer()
	signin()
	.then( function( newtoken ) {
		return sites()
	})
	.then( function( sites ) {
		deferred.resolve( sites )
	} )
	return deferred.promise
}

exports.signin = signin_and_sites_and_users

function checkConfig() {
	// verifying that content folder exists
	fs.exists( "content", function ( exists ) {
		if( !exists ){ fs.mkdir( "content", function() {} ) }
	});
}

function init() {
	checkConfig()
	var deferred = Q.defer()
	var mysites
	read_settings_file()
	.then( function( done ) {
		return signin_and_sites_and_users()
	} )
	.then( function( sites ) {
		mysites = sites
		return users()
	})
	.then( function( users ) {
		deferred.resolve( sites )
	} )
	return deferred.promise
}

exports.init = init


function signout() {
	var deferred = Q.defer()
	message = ""
	call_api_and_parse( request.post, "/api/2.0/auth/signout", message, true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
        else deferred.resolve( "signout" )
	})
	return deferred.promise
}

exports.signout = signout

function adduser( userName, siteLuid, role, publish, contentadmin, siteadmin ) {
    if (typeof siteLuid == 'undefined') siteLuid = _sites_by_URL[ _site ]
	var deferred = Q.defer()
	message = "<tsRequest> \
		<user name=\"" + userName + "\" role=\"" + role + "\" publish=\"" + publish + "\" contentAdmin=\"" + contentadmin + "\" siteAdmin=\"" + siteadmin + "\" suppressGettingStarted=\"True\"" +
		"/> </tsRequest>";
	call_api_and_parse( request.post, "/api/2.0/sites/" + siteLuid + "/users/", message, true, function( error, body, response, result ) {
    	if( error ) deferred.reject( error )
    	else {
	    	var _userId = result.tsResponse.user;
            _userId = JSON.stringify(_userId[0].$.id, null, 2).replace(/[\"]+/g, ""); 
            deferred.resolve(_userId)
        }
	})
    return deferred.promise
}

exports.adduser = adduser

function createsite( sitename, contenturl, adminmode, userquota, storagequotamb ) {
	var deferred = Q.defer()
	message = "<tsRequest> \
		<site name=\"" + sitename + "\" \
		contentUrl=\"" + contenturl + "\" " + 
		// adminMode=\"" + adminmode + "\" \
		// userQuota=\"" + userquota + "\" \
		// storageQuota=\"" + storagequotamb + "\" \
		// disableSubscriptions=\"false\" /> \
	"/></tsRequest>"
	call_api_and_parse( request.post, "/api/2.0/sites/", message, true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
			var _siteLuid = result.tsResponse.site;
			_siteLuid = JSON.stringify(_siteLuid[0].$.id, null, 2).replace(/[\"]+/g, "");      
            sites().then( function() { deferred.resolve(_siteLuid) } )
		}
	})
	return deferred.promise
}

exports.createsite = createsite

function deletesite( sitename ) {
	var siteid = _sites_by_URL[ sitename ]
	var deferred = Q.defer()
	call_api_and_parse( request.del, "/api/2.0/sites/" + siteid, "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
			sites().then( function() { deferred.resolve( "Deletion of site " + sitename + " succesful" ) } )
		}
	})
	return deferred.promise
}

exports.deletesite = deletesite

function createproject( name, description ) {
	var deferred = Q.defer()
	message = "<tsRequest> \
		<project name=\"" + name + "\" \
		description=\"" + description + "\" " + 
		// adminMode=\"" + adminmode + "\" \
		// userQuota=\"" + userquota + "\" \
		// storageQuota=\"" + storagequotamb + "\" \
		// disableSubscriptions=\"false\" /> \
	"/></tsRequest>"
	call_api_and_parse( request.post, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/projects", message, true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else sites( false ).then( function() { deferred.resolve( result.tsResponse.project[0].$.id ) } )
	})
	return deferred.promise
}

exports.createproject = createproject

function deleteproject( name ) {
	var siteid = _sites_by_URL[ _site ]
	var projectid = _projects_by_name[ name ]
	var deferred = Q.defer()
	call_api_and_parse( request.del, "/api/2.0/sites/" + siteid + "/projects/" + projectid, "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else sites( false ).then( function() { deferred.resolve( "deleted project succesfully" ) } )
	})
	return deferred.promise
}

exports.deleteproject = deleteproject

function deletetag( workbook_id, tag ) {
	var deferred = Q.defer()
	call_api_and_parse( request.del, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/workbooks/" + workbook_id + "/tags/" + tag, "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else deferred.resolve( "deletion succesful" )
	})
	return deferred.promise
}

exports.deletetag = deletetag

//
// rest api call: SITES
// returns a promise
//

function sites( add_to_frames ) {
	if( typeof(add_to_frames)==='undefined' ) add_to_frames = true
	var deferred = Q.defer()
	call_api_and_parse( request.get, "/api/2.0/sites?includeProjects=true", "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
			_sites_by_name = {}
	    	_sites_by_URL = {}
	    	var newframe = { "type" : "sites", "content" : {} }
	    	for( i in result.tsResponse.sites[0].site ) {
	    	 	_sites_by_name[ result.tsResponse.sites[0].site[i].$.name ] = result.tsResponse.sites[0].site[i].$.id
	    	 	_sites_by_URL[ result.tsResponse.sites[0].site[i].$.contentUrl ] = result.tsResponse.sites[0].site[i].$.id
	    	 	_site_objects_by_name[ result.tsResponse.sites[0].site[i].$.name ] = result.tsResponse.sites[0].site[i]
	    	 	// commented out because "" is not Default's name, it is its URL
	    	 	// if( result.tsResponse.sites[0].site[i].$.name == "Default" ) {
	    	 	// 	_sites_by_name[ "" ] = result.tsResponse.sites[0].site[i].$.id
	    	 	// 	_site_objects_by_name[ "" ] = result.tsResponse.sites[0].site[i]
	    	 	// }
	    	 	
	    	 	if( add_to_frames ) newframe[ "content" ][ result.tsResponse.sites[0].site[i].$.name ] =  result.tsResponse.sites[0].site[i]
	    	}
	    	if( add_to_frames ) _frames.push( newframe )
	    	projects( false ).then( function () { deferred.resolve( newframe[ "content" ] ) }, function() { console.log( "error while getting projects" ) } )
		}
	})
	return deferred.promise
}

exports.sites = sites

//
//
//
//
//

function projects( add_to_frames ) {
	var deferred = Q.defer()
	if( typeof(add_to_frames)==='undefined' ) add_to_frames = true

	call_api_and_parse( request.get, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/projects", "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
			var newframe = { "type" : "projects", "content" : {} }
			_projects_by_name = {}
			for( i in result.tsResponse.projects[0].project ) {
		    	 	_projects_by_name[ result.tsResponse.projects[0].project[i].$.name ] = result.tsResponse.projects[0].project[i].$.id
		    	 	if( add_to_frames ) newframe[ "content" ][ result.tsResponse.projects[0].project[i].$.name ] = result.tsResponse.projects[0].project[i]
		    }
		    if( add_to_frames ) _frames.push( newframe )
		    deferred.resolve( newframe[ "content" ] )
		}
	})
	
	return deferred.promise
}

exports.projects = projects

//
// rest api call: USERS
// returns a promise
//

exports.users = users

function users( add_to_frames ) {
	if( typeof(add_to_frames)==='undefined' ) add_to_frames = true
	var deferred = Q.defer()
	call_api_and_parse( request.get, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/users", "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
	    	_users_by_name = {}
	    	var newframe = { "type" : "users", "content" : {} }
	    	for( i in result.tsResponse.users[0].user ) {
	    	 	_users_by_name[ result.tsResponse.users[0].user[i].$.name ] = result.tsResponse.users[0].user[i].$.id
	    	 	if( add_to_frames ) newframe[ "content" ][ result.tsResponse.users[0].user[i].$.name ] = result.tsResponse.users[0].user[i]
	    	}
	    	if( add_to_frames ) _frames.push( newframe )
	    	deferred.resolve( newframe[ "content" ] )
		}
	})
	return deferred.promise
}

//
// rest api call: DATASOURCES
// returns a promise
//

exports.datasources = datasources

function datasources( add_to_frames ) {
	if( typeof(add_to_frames)==='undefined' ) add_to_frames = true
	var deferred = Q.defer()
	call_api_and_parse( request.get, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/datasources", "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
	    	_datasources_by_name = {}
	    	var newframe = { "type" : "datasources", "content" : {} }
	    	for( i in result.tsResponse.datasources[0].datasource ) {
	    	 	_datasources_by_name[ result.tsResponse.datasources[0].datasource[i].$.name ] = result.tsResponse.datasources[0].datasource[i].$.id
	    	 	if( add_to_frames ) newframe[ "content" ][ result.tsResponse.datasources[0].datasource[i].$.name ] = result.tsResponse.datasources[0].datasource[i]
	    	}
	    	if( add_to_frames ) _frames.push( newframe )
			deferred.resolve( _datasources_by_name )
		}
	})
	return deferred.promise
}

//
// rest api call: DATASOURCE
// returns a promise
//

function datasource( datasource ) {
	var deferred = Q.defer()
	call_api_and_parse( request.get, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/datasources/" + _datasources_by_name[ datasource ], "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
	    	console.log( "ID=" + result.tsResponse.datasource[0].$.id )
	    	console.log( "NAME=" + result.tsResponse.datasource[0].$.name )
	    	console.log( "TYPE=" + result.tsResponse.datasource[0].$.type )
	    	console.log( "PROJECT=" + result.tsResponse.datasource[0].project[0].$.name )
	    	// console.log( result.tsResponse.datasource[0].tags )
    		for( i=0; i < result.tsResponse.datasource[0].tags.length; i++ ) {
    			if( result.tsResponse.datasource[0].tags[i].tag )
    				console.log( "TAG=" + result.tsResponse.datasource[0].tags[i].tag[0].$.label )
	    	}
	    	deferred.resolve( result )
	    }
	})
	return deferred.promise
}

exports.datasource = datasource

//
// rest api call: WORKBOOKS
// returns a promise
//

function workbooks( user, get_views, add_to_frames ) {
	if( typeof(add_to_frames)==='undefined' ) add_to_frames = true
	if( typeof(get_views)==='undefined' ) get_views = true
	var deferred = Q.defer()
	// console.log( "Looking for workbooks for " + user )
	call_api_and_parse( request.get, "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/users/" + _users_by_name[ user ] + "/workbooks", "", true, function( error, body, response, result ) {
		if( error ) deferred.reject( error )
		else {
	    	_datasources_by_name = {}
	    	var newframe = { "type" : "workbooks", "content" : {} }
	    	for( i in result.tsResponse.workbooks[0].workbook ) {
	    	 	newframe[ "content" ][ result.tsResponse.workbooks[0].workbook[i].$.name ] = result.tsResponse.workbooks[0].workbook[i]
	    	}
	    	if( add_to_frames ) _frames.push( newframe )

	    	// getting views if needed
	    	if( get_views && result.tsResponse.workbooks[0].workbook && result.tsResponse.workbooks[0].workbook.length > 0 ) {
	    		// console.log( "Getting views" )
	    		var promises = []
	    		for( wb in newframe[ "content" ] ) {
	    			// console.log( "Getting views for " + wb )
	    			var deferred_view_request = Q.defer()
	    			promises.push( deferred_view_request.promise );
	    			newframe[ "content" ][wb].$.guessedContentUrl = urlify( newframe[ "content" ][wb].$.name );
	    			(function( def_req, frame, thiswb ) {
		    			doget( "/api/2.0/sites/" + _sites_by_URL[ _site ] + "/workbooks/" + newframe[ "content" ][ thiswb ].$.id + "/views", function( error, body ) {
		    				xml2js.parseString( body, function( err, result ) {
		    					// console.log( "Got views" )
		    					// console.log( result.tsResponse.views[0].view )

		    					// going through the views and guessing the URL
		    					// this guessing is going to be wrong at times, e.g. you have both a "Tableau Software"
		    					// and a "tableauSoftware" workbook, guesses URLs will be indentical while actuals will
		    					// have a _0 to distinguish them

		    					for( index in result.tsResponse.views[0].view ) {
		    						// console.log( result.tsResponse.views[0].view[index] )
		    						result.tsResponse.views[0].view[index].$.guessedContentUrl = urlify( result.tsResponse.views[0].view[index].$.name )
		    					}

			    				frame[ "content" ][ thiswb ].views = result.tsResponse.views[0].view
			    				def_req.resolve( "Got it" )
		    				})
		    			})
		    		})( deferred_view_request, newframe, wb );
	    			// now waiting for all request answers to resolve overall promise
	    			Q.all( promises ).done( function( results ) {
	    				deferred.resolve( newframe[ "content" ] )
	    			})
	    		}
	    	} else { // if get_views
	    		deferred.resolve( newframe[ "content" ] )
	    	}
	    }
	})
	return deferred.promise
}

exports.workbooks = workbooks


function filter( variable, framecommand, jscode ) {
	try {
		var framenumber = framecommand[ "frame" ]
    	if( framecommand[ "command" ] == "pastframe" ) framenumber = _frames.length - framecommand[ "frame" ] - 1
    	var frame = _frames[framenumber]
    	if( framecommand[ "variable" ] ) frame = _variables[ framecommand[ "variable" ] ]

		var newframe = { "type": frame[ "type" ], "content": [] }
		for( elemindex in frame[ "content" ] ) {
			elem = frame[ "content" ][ elemindex ]
			_variables[ variable ] = frame["content"][elemindex]
			try {
				//console.log( "Processing value" )
				//console.log( processValue( jscode ) )
				if( processValue( jscode ) ) newframe[ "content" ][ elem.$.name ] = elem
			}
			catch( error ) { console.log( error ) }
		}
		_frames.push( newframe )
	} catch( error ) { console.log( "Error while processing filter" ); console.log( error ); process.exit() }
}

function collect( variable, framecommand, jscode ) {
	try {
		var framenumber = framecommand[ "frame" ]
    	if( framecommand[ "command" ] == "pastframe" ) framenumber = _frames.length - framecommand[ "frame" ] - 1
    	var frame = _frames[framenumber]
    	if( framecommand[ "variable" ] ) frame = _variables[ framecommand[ "variable" ] ]
		var newframe = { "type": "collection", "content": [] }
		for( elemindex in frame[ "content" ] ) {
			elem = frame[ "content" ][ elemindex ]
			_variables[ variable ] = frame["content"][elemindex]
			try {
				newframe[ "content" ][ processValue( jscode ) ] = processValue( jscode )
			}
			catch( error ) { console.log( error ) }
		}
		_frames.push( newframe )
	} catch( error ) { console.log( "Error while processing collect" ); console.log( error ); process.exit() }
}

//
// Initialization sequence
//

function done( code, errormsg ) {
	console.log( "Exiting with code " + code + " - " + errormsg )
	process.exit( code );
}

function nextcommandstring() { return ""
+ _frames.length + " frames - type 'help' for help" }

function helpstring() { return "\
MENU\n\
====\n\
help ................... Help (this screen)\n\
sites................... List of Sites\n\
use [newsite] .......... Use Site [" + _site + "]\n\
users .................. List of Users\n\
datasources ............ List of Datasources\n\
datasource [ds]......... Read Datasource\n\
workbooks [user]........ List Workbooks for user\n\
frames ................. List data frames\n\
exit ................... Exit" }

process.stdin.resume();
process.stdin.setEncoding('utf8');

function ask(question, callback) {
  var r = rl.createInterface({
    input: process.stdin,
    output: process.stdout})
  r.question( question, function(answer) {
    r.close();
    callback( answer );
  })
}

// make a simple deferred/promise out of the prompt function
var prompter = function(text,options,callback) {
    var deferred = Q.defer();
    promptly.prompt(text, options, function(err, value) {
    	if( err ) console.log( err );
    	callback( value );
       	deferred.resolve(value);
    });
    return deferred.promise;
};

function DumpObjectIndented(obj, indent)
{
  var result = "";
  if (indent == null) indent = "";

  for (var property in obj)
  {
    var value = obj[property];
    if (typeof value == 'string')
      value = "'" + value + "'";
    else if (typeof value == 'object')
    {
      if (value instanceof Array)
      {
        value = "[ " + value + " ]";
      }
      else
      {
        var od = DumpObjectIndented(value, indent + "  ");
        value = "\n" + indent + "{\n" + od + "\n" + indent + "}";
      }
    }
    result += indent + "'" + property + "' : " + value + ",\n";
  }
  return result.replace(/,\n$/, "");
}

function printFrame( i ) {
	console.log( "#" + i + " #-" + (_frames.length-i-1) + " " + Object.keys( _frames[i]["content"] ).length + " " + _frames[i]["type"] )
	k = 1
	for( j in _frames[i]["content"] ) {
		console.log( k + "> " + j )
		k++
	}
}

function printElement( i, j ) {
	console.log( DumpObjectIndented( _frames[i]["content"][ Object.keys( _frames[i]["content"] )[j-1] ], "" ) + "\n" )
}

function processValue( parsed ) {
	try {
		var vars = _variables
		for( v in _variables ) {
			// console.log( "var " + v + " = " + JSON.stringify(_variables[v], null, 4) )
			eval( "var $" + v + " = " + JSON.stringify(_variables[v], null, 4) )
		}
		for( v in _parameters ) {
			// console.log( "var " + v + " = " + JSON.stringify(_variables[v], null, 4) )
			eval( "var $" + v + " = " + JSON.stringify(_parameters[v], null, 4) )
		}
		if( parsed[ "value" ] == "string" ) return parsed[ "literal" ]
		else if( parsed[ "value" ] == "argument" ) return _parameters[ parsed[ "argument" ] ]
		else if( parsed[ "command" ] == "element" ) {
			console.log( "Evaluating element" )
			var framenumber = parsed[ "framecommand" ][ "frame" ]
		    if( parsed[ "framecommand" ][ "command" ] == "pastframe" ) framenumber = _frames.length - parsed[ "framecommand" ][ "frame" ] - 1
			return _frames[ framenumber ][ "content" ][ Object.keys( _frames[framenumber]["content"] )[ parsed[ "index" ] -1] ]
		}
		else if( parsed[ "value" ] == "variable" ) return _variables[ parsed[ "variable" ] ]
		else if( parsed[ "command" ] == "javascript" ) { return eval( parsed[ "code" ] ) }
		else if( parsed[ "command" ] == "parameter" ) { return _parameters[ parsed[ "parameter" ] ] }
	} catch( err ) { console.log( err ); } 
}

function downloadWorkbook( workbookname ) {
	var deferred = Q.defer()
	var value = urlify( workbookname )
	var relativeurl = "/workbooks/" + value + "?format=twb"
	if( _site != "" ) relativeurl = "/t/" + _site + "/workbooks/" + value + "?format=twb"
	
	doget( relativeurl, function( error, body, response ) {
		if( error ) {
			deferred.reject( "error downloading" )
		}
		else
		{
			// console.log( response.headers )
			try {
				var filename = response.headers[ "content-disposition" ].replace( "attachment; filename=\"", "" )
				filename = fileify( filename.substring( 0, filename.length - 1 ) )
				fs.writeFile( "content/" + filename, body, function(err) {
				    if(err) {
				        console.log(err);
				    } else {
				    	console.log( "Downloaded " + relativeurl )
				        deferred.resolve( filename )
				    }
				});
			} catch( error ) { deferred.reject( "error downloading " + error ) }
		}
	} )

	return deferred.promise
}

exports.downloadWorkbook = downloadWorkbook

//
//	PROCESS COMMANDS
//

function processCommand( parsed, interactive ) {
	// console.log( parsed )
	if( typeof(interactive)==='undefined' ) interactive = false
	var deferred = Q.defer()
	try {
		// console.log( parsed )
		if( parsed[ "command" ] == "LU" ) {
	    	return users().then( function () {
	    		if( interactive ) return processCommand( { "command" : "pastframe", "frame": 0 } )
			})
	    }
		else if( parsed[ "command" ] == "LD" ) {
			return datasources().then( function( result ) {
				if( interactive ) return processCommand( { "command" : "pastframe", "frame": 0 } )
	    	} )
	    }
	    else if( parsed[ "command" ] == "LP" ) {
			return projects().then( function( result ) {
				if( interactive ) return processCommand( { "command" : "pastframe", "frame": 0 } )
	    	} )
	    }
		else if( parsed[ "command" ] == "LS" ) {
			return sites().then( function() {
				// if we are in interactive mode, we display these sites now
				if( interactive ) return processCommand( { "command" : "pastframe", "frame": 0 } )
	    	} )
	    }
		else if( parsed[ "command" ] == "CS" ) {
			_site = _site_objects_by_name[ processValue( parsed[ "site" ] ) ].$.contentUrl
			return users().then( datasources )
	    }
	    else if( parsed[ "command" ] == "createsite" ) {
	    	var site = processValue( parsed[ "site" ] )
	    	return createsite( site, site )
	    }
	    else if( parsed[ "command" ] == "adduser" ) {
	    	var user = processValue( parsed[ "user" ] )
	    	var role = processValue( parsed[ "role" ] )
	    	var publish = processValue( parsed[ "publish" ] )
	    	var contentadmin = processValue( parsed[ "contentadmin" ] )
	    	var siteadmin = processValue( parsed[ "siteadmin" ] )
	    	return adduser( user, _sites_by_URL[ _site ], role, publish, contentadmin, siteadmin )
	    }
	    else if( parsed[ "command" ] == "deletesite" ) {
	    	var site = processValue( parsed[ "site" ] )
	    	return deletesite( site, site )
	    }
	    else if( parsed[ "command" ] == "createproject" ) {
	    	var name = processValue( parsed[ "name" ] )
	    	var description = processValue( parsed[ "description" ] )
	    	return createproject( name, description )
	    }
	    else if( parsed[ "command" ] == "deleteproject" ) {
	    	var name = processValue( parsed[ "name" ] )
	    	return deleteproject( name )
	    }
	    else if( parsed[ "command" ] == "deletetag" ) {
	    	var workbook_id = processValue( parsed[ "workbook_id" ] )
	    	var tag = processValue( parsed[ "tag" ] )
	    	return deletetag( workbook_id, tag )
	    }
	    else if( parsed[ "command" ] == "WORKBOOKS" ) {
	    	var user = processValue( parsed[ "user" ] )
	    	return workbooks( user ).then( function() {
				// if we are in interactive mode, we display these workbooks now
				if( interactive ) return processCommand( { "command" : "pastframe", "frame": 0 } )
	    	} )
	    }
	    else if( parsed[ "command" ] == "RDS" ) {
			datasource_name = parsed[ "datasource" ]
	    	return datasource( datasource_name )
	    }
	    else if( parsed[ "command" ] == "F" ) {
			for( i in _frames ) {
				console.log( "#" + i + " #-" + (_frames.length-i-1) + " " + Object.keys( _frames[i]["content"] ).length + " " + _frames[i]["type"] )
			}
			deferred.resolve( "done printing frame" )
	    }
	    else if( parsed[ "command" ] == "H" ) {
			console.log( helpstring() )
			deferred.resolve( "done printing help" )
	    }
	    else if( parsed[ "command" ] == "use" ) {
			_site = _site_objects_by_name[ processValue( parsed[ "site" ] ) ].$.contentUrl
			var r = Q(0);
			r = r.then( signin )
			r = r.then( users )
			r = r.then( datasources )
			r = r.then( projects )
			return r
	    }
	    else if( parsed[ "command" ] == "print" ) {
	    	var value = processValue( parsed[ "value" ] )
	    	console.log( value )
	    	deferred.resolve( "done printing" )
	    }
	    else if( parsed[ "command" ] == "frame" ) {
	    	printFrame( parsed[ "frame" ] )
	    	deferred.resolve( "done printing frame" )
	    }
	     else if( parsed[ "command" ] == "pastframe" ) {
	    	printFrame( _frames.length - parsed[ "frame" ] - 1 )
	    	deferred.resolve( "done printing frame" )
	    }
	    else if( parsed["command"] == "download" ) {
<<<<<<< HEAD
	    	return downloadWorkbook( processValue( parsed[ "value" ] ) )
=======
	    	var value = urlify( processValue( parsed[ "value" ] ) )
	    	var relativeurl = "/workbooks/" + value + "?format=twb"
	    	if( _site != "" ) relativeurl = "/t/" + _site + "/workbooks/" + value + "?format=twb"
	    	
	    	doget( relativeurl, function( error, body, response ) {
	    		if( error ) {
	    			deferred.fail( "error downloading" )
	    		}
	    		else
	    		{
	    			var filename = response.headers[ "content-disposition" ].replace( "attachment; filename=\"", "" )
	    			filename = fileify( filename.substring( 0, filename.length - 1 ) )
	    			fs.writeFile( "content/" + filename, body, function(err) {
					    if(err) {
					        console.log(err);
					    } else {
					    	console.log( "Downloaded " + relativeurl )
					        deferred.resolve( "done downloading" )
					    }
					});
	    		}
	    		
	    	} )
>>>>>>> dad8e42979b086b0eb8ab8811b5bf6f078751275
	    }
	    else if( parsed[ "command" ] == "publishworkbook" ) {
	    	var wb = processValue( parsed[ "workbook" ] )
	    	console.log( "Publishing " + wb )
	    	var filecommand = _binfolder + "\\tabcmd.exe"
	    	var arguments = [ "publish", "content\\" + fileify( wb ) + ".twbx", "-t", _site_objects_by_name[ _site ].$.contentUrl, "--overwrite", "-s", baseurl(), "-u", _admin, "-p", _passwd ]
	    	var publichprocess = child_process.execFile( filecommand, arguments, [], function( error, stdout, stderr ) {
	    		console.log('stdout: ' + stdout);
			    console.log('stderr: ' + stderr);
			    if (error !== null) {
			      console.log('exec error: ' + error);
			    }
	    		console.log( "done with publishing " + wb )
	    		deferred.resolve( "done publishing" )
	    	} )
	    }
	    else if( parsed[ "command" ] == "loop" ) {
	    	var framenumber = parsed[ "framecommand" ][ "frame" ]
	    	if( parsed[ "framecommand" ][ "command" ] == "pastframe" ) framenumber = _frames.length - parsed[ "framecommand" ][ "frame" ] - 1
	    	var frame = _frames[framenumber]
	    	if( parsed[ "framecommand" ][ "variable" ] ) frame = _variables[ parsed[ "framecommand" ][ "variable" ] ]
	    	var r = Q(0);
	    	for( i in frame["content"] )( function(i) {
	    		r = r.then( function() {
	    			// console.log( "with i=" + i + ", " + parsed[ "variable" ] + "=" )
					// console.log( _frames[framenumber]["content"][i] )
					var s = Q(0);
		    		_variables[ parsed[ "variable" ] ] = frame["content"][i]
		    		for( c in parsed[ "commands" ] )( function(c) {
		    			s = s.then( function() {
		    				if( parsed[ "value" ] ) console.log( processValue( parsed ) )
		    				else return processCommand( parsed[ "commands" ][c] ) } )
		    		})(c)
		    		return s
	    		})
    		})(i)
    		return r
	    }
	    else if( parsed[ "command" ] == "filter" ) {
	    	return filter( parsed[ "variable" ], parsed[ "framecommand" ], parsed[ "javascript" ] )
	    }
	    else if( parsed[ "command" ] == "collect" ) {
	    	return collect( parsed[ "variable" ], parsed[ "framecommand" ], parsed[ "javascript" ] )
	    }
	    else if( parsed[ "command" ] == "javascript" ) {
	    	// setting local variables
	    	for( v in _variables ) {
				eval( "var $" + v + " = " + JSON.stringify(_variables[v], null, 4) )
			}
			for( v in _parameters ) {
				eval( "var $" + v + " = " + JSON.stringify(_parameters[v], null, 4) )
			}
	    	eval( parsed[ "code" ] );
	    	deferred.resolve( "done with javascript" )
	    }
	    else if( parsed[ "value" ] ) {
	    	console.log( JSON.stringify( processValue( parsed ), null, 4 ) )
	    	deferred.resolve( "done with value" )
	    }
	    else if( parsed[ "command" ] == "assign" ) {
	    	var r = Q(0);

	    	r = r.then( function() { return processCommand( parsed[ "listcommand" ] ) } )
	    	.then( function() { _variables[ parsed[ "variable" ] ] = _frames[ _frames.length - 1 ] } )

	    	return r
	    }
	    else if( parsed[ "parameter" ] ) {
	    	deferred.resolve( "passing parameter" )
	    }
	    else if( parsed[ "command" ] == "X" ) done()
	} catch( err ) { console.log( err ) }
	
	return deferred.promise
}

function nextcommand() {
	console.log( nextcommandstring() )
	console.log( "" )
	ask( "> ", function( result ) {
		try {
			var parsed = parser.parse( result )
			// console.log( parsed )
			var q = Q(0);
			for( i in parsed )( function( i ) {
				q = q.then( function() { return processCommand( parsed[i], true ) } )
			})(i)
			q.then( function() { nextcommand() } )
			return q
		}
		catch( err ) { console.log( err ); nextcommand() } 
	})
}

function interactiveloop() {

	console.log( "\n\
#####  ##  ###  ###   ##  #####\n\
  #   #  # #  # #  # #  #   #\n\
  #   #### ###  ###  ####   #\n\
  #   #  # #  # #  # #  #   #\n\
  #   #  # ###  #  # #  #   #\n\
" )
	console.log( "TABleau Rest Api Tool" )
	nextcommand()
}

function findFirstProperty( obj, attribute )
{
    for (var k in obj)
    {
        if (typeof obj[k] == "object" && obj[k] !== null) {
            var foo = findProperty(obj[k], attribute) 
            if( foo ) return foo
        }
        else if( k == attribute ) return obj[k]
    }
	return undefined
}

function findAnyProperty( obj, attribute, value )
{
    for (var k in obj)
    {
        if (typeof obj[k] == "object" && obj[k] !== null) {
            var foo = findAnyProperty(obj[k], attribute, value ) 
            if( foo ) return foo
        }
        else if( k == attribute && obj[k] == value ) return true
    }
	return undefined
}

//
// We shouldn't have to do urlify - the RESTapi should give us a contentUrl field for views, workbooks, etc.
//

function urlify( name ) {
	return name.replace( / /g, '' ).replace( /\//g, '' ).replace( /\?/g, '' ).replace( /\./g, '_' )
}

exports.urlify = urlify

function fileify( name ) {
	return name.replace( /\//g, '-' ).replace( /\?/g, '-' )
}

function process_commands( commands_string ) {
	var parsed = parser.parse( commands_string )
	var q = Q(0);
	for( i in parsed )( function( i ) {
		q = q.then( function() { return processCommand( parsed[i] ) } )
	})(i)
	return q
}

exports.process_commands = process_commands

//
// We are running with "node tabrat", either interactive or with .tr script
//

if( require.main === module ) {
	checkConfig()
	read_settings_file()
	.then( function( done ) {
		return signin_and_sites_and_users()
	} )
	.then( function( result ) {
		//console.log( "Read " + Object.keys( result ).length + " sites" )
		return users()
	})
	.then( function( result ) {
		//console.log( "Read " + Object.keys( result ).length + " users" )
		return datasources()
	})
	.then( function( result ) {
		//console.log( "Read " + Object.keys( result ).length + " users" )
		return projects()
	})
	.then( function( result ) {
		//console.log( "Read " + Object.keys( result ).length + " datasources" )

		if( process.argv[2] ) {
			// test if we have a .tr script
			fs.exists( process.argv[2], function( exists ) {
				if( exists ) {
					// setting parameters
					for( a=3; a<process.argv.length; a++ ) _parameters[ a-2 ] = process.argv[a]

					var stream = fs.createReadStream( process.argv[2] )
					stream.pipe( bl( function( err, data ) {
						var script = data.toString()
						try {
							// console.log( "Parsing file...")
							var parsed = parser.parse( script )
							// console.log( parsed )

							// now prompting for missing parameters
							var parameters = parsed.filter( function( elem ) { return !( typeof( elem[ "parameter" ] ) === 'undefined' ) } )			
							var commands = parsed.filter( function( elem ) { return ( typeof( elem[ "parameter" ] ) === 'undefined' ) } )

							// console.log( commands )		

							var q = Q(0);

							for( i in parameters ) ( function(i) {
								var knownval = _parameters[ parameters[i][ "parameter" ] ]
								if( typeof( knownval ) === 'undefined' ) {
									q = q.then( function() {
										return prompter( parameters[i][ "question" ] + " (" + parameters[i][ "default" ] + ") >", { "default" : parameters[i][ "default" ] }, function( result ) {
											if( result === '' ) _parameters[ parameters[i][ "parameter" ] ] = parameters[i][ "default" ]
											else  _parameters[ parameters[i][ "parameter" ] ] = result
										} )
									} )
								}
							})(i)

							for( i in commands )( function( i ) {
								q = q.then( function() { return processCommand( commands[i] ) },
									function( error ) { console.log( "Error on line " + (i-1) + ", code " + error.$.code + " - " + error.detail )
										return processCommand( commands[i] ) } )
							})(i)

							q.then( function() { done( 0, "SUCCESS" ) }, function( error )
								{ done( error.$.code, error.detail ) } )
							return q
						}
						catch( err ) { console.log( err ) } 
					}))
				}
				else
				{
					interactiveloop()
				}
			} )
		}
		else
		{
			interactiveloop()
		}
	})
}