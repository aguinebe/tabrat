
var tabrat = require( "./tabrat" )

// global variables where you'll keep arrays of objects for later processing

var mysites
var myusers
var myprojects

// chain of calls to the REST API

tabrat.init()
.then( function( sites ) {
	mysites = sites
	return tabrat.users()
})
.then( function( users ) {
	myusers = users
	return tabrat.projects()
})
.then( function( projects ) {
	myprojects = projects
	// you want to try / catch your code here otherwise you won't see errors
	try {
		console.log( "Here are the sites:" )
		console.log( mysites )
		console.log( "Here are the users:" )
		console.log( myusers )
		console.log( "Here are the projects:" )
		console.log( myusers )
	} catch( error ) { console.log( error ); process.exit() }
	process.exit()
})
